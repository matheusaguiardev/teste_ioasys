﻿**TESTE PARA DESENVOLVEDOR ANDROID IOASYS**

---

## Dependencias

Breve descrição das principais dependências utilizadas.

1. **Koin**, para injeção de dependência.
2. **Retrofit2** + **logging-interceptor** para consumir a api proposta no desafio e para ler o log do request.
3. **Coroutines**, para executar processos suspensiveis e não bloqueantes.
4. **Hawk**, para armazenamento encriptado de dados pequenos.*
5. **Androidx**, jetpack de dependências, disponibilizado pela Google.
6. **Picasso**, para realizar downloads de imagens, no caso, fornecidas pela API
---

## Como rodar o projeto

1. Baixar este projeto para seu computador.
2. Abrir o projeto com o android studio.
2.1 File > Open > Encontre a pasta onde você salvou o projeto
3. Se você conseguir rodar um emulador com o android studio é só apertar "Run 'app'" se não, espete o cabo no seu aparelho android e plugue no computador e dê o play.
4. Selecione o seu dispositivo que queria rodar o projeto.
5. Observação: Para efeito de agilidade, programaticalmente foi colocado um usuario e senha em um método que retira o valor dos campos de textos. Para utilizar com mais usuários
remova a String com o usuario e senha e descomente o metodo que ler os campos.
---

## Se eu tivesse mais tempo?

1. Se eu tivesse mais tempo eu concluiria o projeto (funcionalidade de pesquisa/filtro + tela de detalhe)
1.1 Infelizmente eu estou com uma máquina bem ruim atualmente e esta travando muito, por falta de memória.
2. Faria o download de imagens por OnDemand para cada empresa da lista.
3. Melhoraria o layout
4. Implementaria os testes unitários. (mockando os repositórios e testando suas lógicas)
