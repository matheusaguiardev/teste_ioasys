package br.com.aguiar.ioasysaguiar.domain.model.picasso

import br.com.aguiar.ioasysaguiar.domain.repository.PicassoRepository
import com.squareup.picasso.RequestCreator

class PicassoInteractor(
    private val picassoRepository: PicassoRepository
) {

    fun fetchImage(url: String): RequestCreator {
        return picassoRepository.fetchImageWithPicasso(url)
    }

}