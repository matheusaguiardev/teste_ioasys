package br.com.aguiar.ioasysaguiar.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import br.com.aguiar.ioasysaguiar.domain.model.enterprise.CompanyInteractor
import br.com.aguiar.ioasysaguiar.domain.model.enterprise.CompanyResponse
import br.com.aguiar.ioasysaguiar.domain.model.picasso.PicassoInteractor
import com.squareup.picasso.RequestCreator
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class MainViewModel(
    private val interactorCompany: CompanyInteractor,
    private val interactorPicasso: PicassoInteractor
) : ViewModel(), CoroutineScope {

    private var companyJob = Job()

    override val coroutineContext: CoroutineContext = Dispatchers.Main + companyJob

    private val companyList = MutableLiveData<CompanyResponse>()
    fun companyList(): LiveData<CompanyResponse> = companyList

    private val loading = MutableLiveData<Boolean>()
    fun loading(): LiveData<Boolean> = loading

    fun fetchCompanies() {
        companyJob = launch {
            loading.value = true
            val result = interactorCompany()
            companyList.value = result
            loading.value = false
        }
    }

    fun fetchImage(url: String): RequestCreator {
        return interactorPicasso.fetchImage(url)
    }

    override fun onCleared() {
        super.onCleared()
        companyJob.cancel()
    }

}