package br.com.aguiar.ioasysaguiar.ui.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.RequestCreator
import kotlinx.android.synthetic.main.itemview_company.view.*

class CompanyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bind(name: String, description: String, country: String, image: RequestCreator?) {
        itemView.nameCompany.text = name
        itemView.descriptionCompany.text = description
        itemView.countryCompany.text = country

        image?.centerInside()
            ?.into(itemView.imageCompany)
    }


}