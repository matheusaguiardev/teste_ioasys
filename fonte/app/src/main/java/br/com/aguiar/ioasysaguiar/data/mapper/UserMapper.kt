package br.com.aguiar.ioasysaguiar.data.mapper

import br.com.aguiar.ioasysaguiar.data.model.UserJson
import br.com.aguiar.ioasysaguiar.domain.model.login.User

fun User.toJson() = UserJson(
    email = this.email,
    password = this.password
)