package br.com.aguiar.ioasysaguiar.data.mapper

import br.com.aguiar.ioasysaguiar.data.model.CompanyResponseJson
import br.com.aguiar.ioasysaguiar.domain.model.enterprise.CompanyResponse

fun CompanyResponseJson.toDomain() = CompanyResponse(
    this.enterprises.map { it.toDomain() }
)