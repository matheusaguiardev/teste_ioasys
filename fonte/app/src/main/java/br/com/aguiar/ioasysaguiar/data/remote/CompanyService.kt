package br.com.aguiar.ioasysaguiar.data.remote

import br.com.aguiar.ioasysaguiar.data.model.CompanyResponseJson
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET

interface CompanyService {

    @GET("enterprises")
    fun fetchCompanies(): Deferred<Response<CompanyResponseJson>>

}