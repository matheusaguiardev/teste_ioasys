package br.com.aguiar.ioasysaguiar.domain.model.login

data class User(
    val email: String,
    val password: String
)