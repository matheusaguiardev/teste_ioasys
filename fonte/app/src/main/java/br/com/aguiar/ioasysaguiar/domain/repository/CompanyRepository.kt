package br.com.aguiar.ioasysaguiar.domain.repository

import br.com.aguiar.ioasysaguiar.domain.model.enterprise.CompanyResponse

interface CompanyRepository {

    suspend fun fetchCompanies(): CompanyResponse

}