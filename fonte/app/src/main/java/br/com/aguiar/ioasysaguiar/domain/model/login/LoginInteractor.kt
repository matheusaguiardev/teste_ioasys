package br.com.aguiar.ioasysaguiar.domain.model.login

import br.com.aguiar.ioasysaguiar.domain.repository.LoginRepository

class LoginInteractor(
    private val loginRepository: LoginRepository
) {

    suspend operator fun invoke(
        email: String,
        password: String
    ) : LoginResponse {
        return loginRepository.login(User(email, password))
    }

}