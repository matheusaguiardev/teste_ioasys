package br.com.aguiar.ioasysaguiar.ui.di

import br.com.aguiar.ioasysaguiar.data.remote.CompanyService
import br.com.aguiar.ioasysaguiar.data.remote.LoginService
import br.com.aguiar.ioasysaguiar.data.utils.Constants.BASEURL
import br.com.aguiar.ioasysaguiar.domain.repository.HawkRepository
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

val remoteModule = module {
    single { createOkHttpClient(get()) }
    single { createWebService<LoginService>(get()) }
    single { createWebService<CompanyService>(get()) }

}


fun createOkHttpClient(hawk: HawkRepository): OkHttpClient {
    val httpLoggingInterceptor = HttpLoggingInterceptor()
    httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

    val tokenInterceptor = Interceptor { chain ->
        val newRequest = chain.request().newBuilder()
            .addHeader("access-token", hawk.getToken())
            .build()
        chain.proceed(newRequest)

    }

    val clientInterceptor = Interceptor { chain ->
        val newRequest = chain.request().newBuilder()
            .addHeader("client", hawk.getClientKey())
            .build()
        chain.proceed(newRequest)
    }

    val uidInterceptor = Interceptor { chain ->
        val newRequest = chain.request().newBuilder()
            .addHeader("uid", hawk.getRemoteUUID())
            .build()
        chain.proceed(newRequest)
    }


    return OkHttpClient.Builder()
        .connectTimeout(60L, TimeUnit.SECONDS)
        .readTimeout(60L, TimeUnit.SECONDS)
        .addInterceptor(httpLoggingInterceptor)
        .addInterceptor(tokenInterceptor)
        .addInterceptor(clientInterceptor)
        .addInterceptor(uidInterceptor)
        .build()
}


inline fun <reified T> createWebService(okHttpClient: OkHttpClient): T {
    val retrofit = Retrofit.Builder()
        .baseUrl(BASEURL)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .client(okHttpClient)
        .build()
    return retrofit.create(T::class.java)
}