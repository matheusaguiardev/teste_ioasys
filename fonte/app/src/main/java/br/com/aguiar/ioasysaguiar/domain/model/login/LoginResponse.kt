package br.com.aguiar.ioasysaguiar.domain.model.login

import br.com.aguiar.ioasysaguiar.data.utils.Constants.INTERNAL_ERROR

data class LoginResponse(
    val success: Boolean,
    val errors: List<String>? = emptyList(),
    val httpCode: Int? = INTERNAL_ERROR
)