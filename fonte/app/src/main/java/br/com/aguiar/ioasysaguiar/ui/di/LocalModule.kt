package br.com.aguiar.ioasysaguiar.ui.di

import android.content.Context
import br.com.aguiar.ioasysaguiar.data.local.HawkDataRepository
import br.com.aguiar.ioasysaguiar.domain.repository.HawkRepository
import com.orhanobut.hawk.Hawk
import com.orhanobut.hawk.HawkBuilder
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val localModule = module {
    single { createHawkInstance(androidContext()) }
    single { HawkDataRepository() as HawkRepository }
}

fun createHawkInstance(context: Context): HawkBuilder = Hawk.init(context)