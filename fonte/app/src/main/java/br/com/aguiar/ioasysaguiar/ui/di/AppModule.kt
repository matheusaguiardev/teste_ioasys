package br.com.aguiar.ioasysaguiar.ui.di

val myModule = listOf(
    localModule,
    remoteModule,
    repositoryModule,
    interactorModule,
    viewModelModule
)