package br.com.aguiar.ioasysaguiar.data.model

import com.google.gson.annotations.SerializedName

data class LoginResponseJson(
    @SerializedName(value = "success") val success: Boolean,
    @SerializedName(value = "errors") val errors: List<String>
)