package br.com.aguiar.ioasysaguiar.domain.repository

interface HawkRepository {

    fun saveToken(token: String)

    fun saveRemoteUUID(uuidRemote: String)

    fun saveClientKey(clientKey: String)

    fun getToken(): String

    fun getRemoteUUID(): String

    fun getClientKey(): String

}