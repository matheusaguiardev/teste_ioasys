package br.com.aguiar.ioasysaguiar.data.model

import com.google.gson.annotations.SerializedName

data class UserJson(
    @SerializedName(value = "email") val email: String,
    @SerializedName(value = "password") val password: String
)