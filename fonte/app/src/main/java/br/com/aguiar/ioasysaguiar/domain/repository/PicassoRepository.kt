package br.com.aguiar.ioasysaguiar.domain.repository

import com.squareup.picasso.RequestCreator

interface PicassoRepository {

    fun fetchImageWithPicasso(url: String): RequestCreator

}