package br.com.aguiar.ioasysaguiar.ui.di

import br.com.aguiar.ioasysaguiar.domain.model.enterprise.CompanyInteractor
import br.com.aguiar.ioasysaguiar.domain.model.login.LoginInteractor
import br.com.aguiar.ioasysaguiar.domain.model.picasso.PicassoInteractor
import org.koin.dsl.module

val interactorModule = module {
    single { LoginInteractor(get()) }
    single { CompanyInteractor(get()) }
    single { PicassoInteractor(get()) }
}