package br.com.aguiar.ioasysaguiar.ui.di

import br.com.aguiar.ioasysaguiar.data.repository.CompanyDataRepository
import br.com.aguiar.ioasysaguiar.data.repository.LoginDataRepository
import br.com.aguiar.ioasysaguiar.data.repository.PicassoDataRepository
import br.com.aguiar.ioasysaguiar.domain.repository.CompanyRepository
import br.com.aguiar.ioasysaguiar.domain.repository.LoginRepository
import br.com.aguiar.ioasysaguiar.domain.repository.PicassoRepository
import org.koin.dsl.module

val repositoryModule = module {
    single { LoginDataRepository(get(), get()) as LoginRepository }
    single { PicassoDataRepository() as PicassoRepository }
    single { CompanyDataRepository(get()) as CompanyRepository }
}