package br.com.aguiar.ioasysaguiar.ui.home

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import br.com.aguiar.ioasysaguiar.R
import br.com.aguiar.ioasysaguiar.domain.model.enterprise.CompanyResponse
import br.com.aguiar.ioasysaguiar.ui.adapter.CompanyListAdapter
import br.com.aguiar.ioasysaguiar.ui.extension.toGone
import br.com.aguiar.ioasysaguiar.ui.extension.toVisible
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.androidx.viewmodel.ext.android.viewModel


class MainActivity : AppCompatActivity() {

    private val viewModel: MainViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        viewModel.companyList().observe(this, Observer(::companiesObserver))
        viewModel.loading().observe(this, Observer(::loadingObserver))
        viewModel.fetchCompanies()
    }

    private fun companiesObserver(list: CompanyResponse?) {
        list?.let {
            setupRecycler(it)
        }
    }

    private fun setupRecycler(list: CompanyResponse) {
        val layoutManager = LinearLayoutManager(this)
        enterpriseList.layoutManager = layoutManager

        /*
        list.enterprises.map {
            it.urlImage = viewModel
            .fetchImage(BASEURL + it.photoEndPoint?.substring(1))
        }
        */

        enterpriseList.adapter = CompanyListAdapter(list.enterprises)

        enterpriseList.addItemDecoration(
            DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        )
    }

    //(lembrete sobre o if) pode parecer estranho, mas o show pode ser null
    private fun loadingObserver(show: Boolean?) {
        if (show == true) {
            showLoading()
        } else {
            hideLoading()
        }
    }

    private fun showLoading() {
        loading.toVisible()
        enterpriseList.toGone()
    }

    private fun hideLoading() {
        loading.toGone()
        enterpriseList.toVisible()
    }

}
