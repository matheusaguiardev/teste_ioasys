package br.com.aguiar.ioasysaguiar.data.repository

import br.com.aguiar.ioasysaguiar.data.mapper.toJson
import br.com.aguiar.ioasysaguiar.data.model.LoginResponseJson
import br.com.aguiar.ioasysaguiar.data.model.UserJson
import br.com.aguiar.ioasysaguiar.data.remote.LoginService
import br.com.aguiar.ioasysaguiar.data.utils.Constants.CLIENT_KEY
import br.com.aguiar.ioasysaguiar.data.utils.Constants.TOKEN_ID
import br.com.aguiar.ioasysaguiar.data.utils.Constants.UUID_KEY
import br.com.aguiar.ioasysaguiar.domain.model.login.LoginResponse
import br.com.aguiar.ioasysaguiar.domain.model.login.User
import br.com.aguiar.ioasysaguiar.domain.repository.HawkRepository
import br.com.aguiar.ioasysaguiar.domain.repository.LoginRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.withContext
import retrofit2.Response

class LoginDataRepository(
    private val service: LoginService,
    private val localPreferences: HawkRepository
) : LoginRepository {

    override suspend fun login(user: User): LoginResponse {
        val result = withContext(Dispatchers.IO) {
            async { loginApi(user.toJson()) }
        }

        val response = result.await()

        val token = response.headers().get(TOKEN_ID) ?: ""
        val uuid = response.headers().get(UUID_KEY) ?: ""
        val client = response.headers().get(CLIENT_KEY) ?: ""

        localPreferences.saveToken(token)
        localPreferences.saveRemoteUUID(uuid)
        localPreferences.saveClientKey(client)

        return LoginResponse(
            response.body()?.success ?: false,
            response.body()?.errors ?: emptyList(),
            response.code()

        )
    }

    private suspend fun loginApi(user: UserJson): Response<LoginResponseJson> {
        return service
            .login(user)
            .await()
    }

}