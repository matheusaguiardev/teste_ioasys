package br.com.aguiar.ioasysaguiar.domain.model.enterprise

import br.com.aguiar.ioasysaguiar.domain.repository.CompanyRepository

class CompanyInteractor(
    private val repository: CompanyRepository
) {

    suspend operator fun invoke(): CompanyResponse {
        return repository.fetchCompanies()
    }

}