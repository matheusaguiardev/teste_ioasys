package br.com.aguiar.ioasysaguiar.data.mapper

import br.com.aguiar.ioasysaguiar.data.model.CompanyJson
import br.com.aguiar.ioasysaguiar.domain.model.enterprise.Company

fun CompanyJson.toDomain() = Company(
    enterpriseName = enterpriseName,
    description = description,
    country = country,
    photoEndPoint = photoEndPoint
)