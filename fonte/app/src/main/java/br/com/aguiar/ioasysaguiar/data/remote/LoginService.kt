package br.com.aguiar.ioasysaguiar.data.remote

import br.com.aguiar.ioasysaguiar.data.model.LoginResponseJson
import br.com.aguiar.ioasysaguiar.data.model.UserJson
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface LoginService {

    @POST("users/auth/sign_in")
    fun login(@Body user: UserJson): Deferred<Response<LoginResponseJson>>

}