package br.com.aguiar.ioasysaguiar.domain.model.enterprise

import com.squareup.picasso.RequestCreator

data class Company(
    val enterpriseName: String,
    val description: String,
    val country: String,
    val photoEndPoint: String?
) {
    var urlImage: RequestCreator? = null

}