package br.com.aguiar.ioasysaguiar.data.repository

import br.com.aguiar.ioasysaguiar.domain.repository.PicassoRepository
import com.squareup.picasso.Picasso
import com.squareup.picasso.RequestCreator

class PicassoDataRepository : PicassoRepository {

    override fun fetchImageWithPicasso(url: String): RequestCreator {
        return downloadImage(url)
    }

    private fun downloadImage(url: String): RequestCreator {
        return Picasso.get()
            .load(url)
            .centerInside()
    }

}