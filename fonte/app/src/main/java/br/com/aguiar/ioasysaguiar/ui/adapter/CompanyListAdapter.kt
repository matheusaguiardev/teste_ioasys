package br.com.aguiar.ioasysaguiar.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.aguiar.ioasysaguiar.R
import br.com.aguiar.ioasysaguiar.domain.model.enterprise.Company
import br.com.aguiar.ioasysaguiar.ui.viewholder.CompanyViewHolder

class CompanyListAdapter(private val dataSource: List<Company>) : RecyclerView.Adapter<CompanyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CompanyViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.itemview_company, parent, false)
        return CompanyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return dataSource.size
    }

    override fun onBindViewHolder(holder: CompanyViewHolder, position: Int) {
        with(dataSource[position]) {
            holder.bind(enterpriseName, description, country, urlImage)
        }
    }
}