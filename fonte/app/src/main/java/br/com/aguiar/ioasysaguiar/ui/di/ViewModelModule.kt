package br.com.aguiar.ioasysaguiar.ui.di

import br.com.aguiar.ioasysaguiar.ui.home.MainViewModel
import br.com.aguiar.ioasysaguiar.ui.login.LoginViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
     viewModel { LoginViewModel(get()) }
    viewModel { MainViewModel(get(), get()) }
}