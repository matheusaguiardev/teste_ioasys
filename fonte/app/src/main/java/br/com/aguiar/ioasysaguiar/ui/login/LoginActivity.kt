package br.com.aguiar.ioasysaguiar.ui.login

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import br.com.aguiar.ioasysaguiar.R
import br.com.aguiar.ioasysaguiar.data.utils.Constants.HTTP_SUCCESS
import br.com.aguiar.ioasysaguiar.domain.model.login.LoginResponse
import br.com.aguiar.ioasysaguiar.ui.home.MainActivity
import br.com.aguiar.ioasysaguiar.ui.extension.toGone
import br.com.aguiar.ioasysaguiar.ui.extension.toInvisible
import br.com.aguiar.ioasysaguiar.ui.extension.toVisible
import kotlinx.android.synthetic.main.activity_login.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class LoginActivity : AppCompatActivity() {

    private val viewModel: LoginViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        signInAction.setOnClickListener { signInAction() }
        viewModel.loginResponse().observe(this, Observer(::loginResultObserver))
        supportActionBar?.hide()
    }

    private fun loginResultObserver(result: LoginResponse?) {
        result?.let { login ->
            if (login.success && login.httpCode == HTTP_SUCCESS) {
                startActivity(
                    Intent(this, MainActivity::class.java)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                )
            } else {
                Toast.makeText(
                    this,
                    if (login.errors?.isNotEmpty() == true) login.errors.first() else "Falha ao realizar o login",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
        progressObserver(show = false)
    }

    private fun progressObserver(show: Boolean) {
        if (show) {
            showLoading()
        } else {
            hideLoading()
        }
    }

    private fun signInAction() {
        // if (validateInput()) {
        progressObserver(show = true)
        viewModel.signIn(toExtractEmail(), toExtractPassword())
        // }
    }

    private fun toExtractEmail() = "testeapple@ioasys.com.br" //emailEdit.text.toString()
    private fun toExtractPassword() = "12341234" //passwordEdit.text.toString()

    private fun validateInput(): Boolean {
        return when {
            toExtractEmail().isEmpty() -> {
                emailEdit.error = getString(R.string.text_email_required)
                false
            }
            toExtractPassword().isEmpty() -> {
                passwordEdit.error = getString(R.string.text_password_required)
                false
            }
            else -> true
        }
    }

    private fun showLoading() {
        loading.toVisible()
        emailEdit.toInvisible()
        passwordEdit.toInvisible()
    }

    private fun hideLoading() {
        loading.toGone()
        emailEdit.toVisible()
        passwordEdit.toVisible()
    }
}
