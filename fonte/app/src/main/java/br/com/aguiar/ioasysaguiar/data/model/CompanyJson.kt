package br.com.aguiar.ioasysaguiar.data.model

import com.google.gson.annotations.SerializedName

data class CompanyJson(
    @SerializedName(value = "enterprise_name") val enterpriseName: String,
    @SerializedName(value = "description") val description: String,
    @SerializedName(value = "country") val country: String,
    @SerializedName(value = "photo") val photoEndPoint: String
)