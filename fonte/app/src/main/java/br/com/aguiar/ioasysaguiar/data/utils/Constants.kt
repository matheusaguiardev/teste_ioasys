package br.com.aguiar.ioasysaguiar.data.utils

object Constants {
    const val HTTP_SUCCESS = 200
    const val INTERNAL_ERROR = 501

    const val TOKEN_ID = "access-token"
    const val UUID_KEY = "uid"
    const val CLIENT_KEY = "client"

    const val BASEURL = "http://empresas.ioasys.com.br/api/v1/"

}