package br.com.aguiar.ioasysaguiar.data.model

import com.google.gson.annotations.SerializedName

data class CompanyResponseJson(
    @SerializedName(value = "enterprises") val enterprises: List<CompanyJson>
)