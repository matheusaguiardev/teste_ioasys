package br.com.aguiar.ioasysaguiar.domain.model.enterprise

class CompanyResponse(
    val enterprises: List<Company>
)