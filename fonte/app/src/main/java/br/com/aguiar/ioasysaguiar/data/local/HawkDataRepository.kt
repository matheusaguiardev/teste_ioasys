package br.com.aguiar.ioasysaguiar.data.local

import br.com.aguiar.ioasysaguiar.domain.repository.HawkRepository
import com.orhanobut.hawk.Hawk

class HawkDataRepository : HawkRepository {

    companion object {
        const val TOKEN_KEY = "TOKEN_KEY"
        const val UUID_KEY = "UUID_KEY"
        const val CLIENT_KEY = "CLIENT_KEY"
    }

    override fun saveToken(token: String) {
        Hawk.put(TOKEN_KEY, token)
    }

    override fun saveRemoteUUID(uuidRemote: String) {
        Hawk.put(UUID_KEY, uuidRemote)
    }

    override fun saveClientKey(clientKey: String) {
        Hawk.put(CLIENT_KEY, clientKey)
    }

    override fun getToken(): String {
        return Hawk.get(TOKEN_KEY, "")
    }

    override fun getRemoteUUID(): String {
        return Hawk.get(UUID_KEY, "")
    }

    override fun getClientKey(): String {
        return Hawk.get(CLIENT_KEY, "")
    }

}