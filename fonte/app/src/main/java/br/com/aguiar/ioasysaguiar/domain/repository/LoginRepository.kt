package br.com.aguiar.ioasysaguiar.domain.repository

import br.com.aguiar.ioasysaguiar.domain.model.login.LoginResponse
import br.com.aguiar.ioasysaguiar.domain.model.login.User

interface LoginRepository {

    suspend fun login(user: User): LoginResponse

}