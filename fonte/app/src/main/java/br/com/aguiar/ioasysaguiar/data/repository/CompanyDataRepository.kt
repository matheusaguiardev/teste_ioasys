package br.com.aguiar.ioasysaguiar.data.repository

import br.com.aguiar.ioasysaguiar.data.mapper.toDomain
import br.com.aguiar.ioasysaguiar.data.model.CompanyResponseJson
import br.com.aguiar.ioasysaguiar.data.remote.CompanyService
import br.com.aguiar.ioasysaguiar.domain.model.enterprise.CompanyResponse
import br.com.aguiar.ioasysaguiar.domain.repository.CompanyRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.withContext

class CompanyDataRepository(
    private val service: CompanyService
) : CompanyRepository {

    override suspend fun fetchCompanies(): CompanyResponse {
        val result = withContext(Dispatchers.IO) {
            async { companyApi() }
        }.await()
        return result.toDomain()
    }

    private suspend fun companyApi(): CompanyResponseJson {
        val result = service.fetchCompanies().await()
        return result.body() ?: CompanyResponseJson(emptyList())
    }

}