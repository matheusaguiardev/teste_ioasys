package br.com.aguiar.ioasysaguiar.ui.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import br.com.aguiar.ioasysaguiar.domain.model.login.LoginInteractor
import br.com.aguiar.ioasysaguiar.domain.model.login.LoginResponse
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

class LoginViewModel(
    val interactor: LoginInteractor
) : ViewModel(), CoroutineScope {

    private var loginJob = Job()

    override val coroutineContext: CoroutineContext = Dispatchers.Main + loginJob

    private val loginResponse = MutableLiveData<LoginResponse>()
    fun loginResponse(): LiveData<LoginResponse> = loginResponse

    fun signIn(
        email: String,
        password: String
    ) {

        loginJob = launch {
            val result = interactor(email, password)
            loginResponse.value = result
        }
    }

    override fun onCleared() {
        super.onCleared()
        loginJob.cancel()
    }


}